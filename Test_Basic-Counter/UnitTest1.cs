﻿using System;
using Basic_Counter;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test_Basic_Counter
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
            public void Bouton1()
            {
                Assert.AreEqual(0, Compte.Decremente());
                Assert.AreEqual(30, Compte.Decremente());
            }
            public void Bouton2()
            {
                Assert.AreEqual(10, Compte.Incremente());
            }
            public void Bouton3()
            {
                Assert.AreEqual(3, Compte.Numero());
            }
    }
}
