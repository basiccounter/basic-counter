﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basic_Counter
{
    public class Compte
    {
        static int compter = 0;
        public static string Numero()
        {
            return "0";
        }

        public static int Decremente()
        {
            if (compter == 0)
            {
                return compter;
            }
            else
            {
                compter = compter - 1;
                return compter;
            }
        }

        public static int Incremente()
        {
            compter = compter + 1;
            return compter;
        }
    }
}
